from sklearn.gaussian_process import GaussianProcessRegressor
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split as tts
from sklearn import svm
from sklearn.linear_model import SGDRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.cross_decomposition import PLSRegression
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import r2_score
import xgboost


def punish(x):
    if x <= 60:
        return 0.96
    elif x < 60 and x <= 90:
        return 1
    elif x > 90 and x <= 110:
        return 0.9
    elif x > 110 and x <= 140:
        return 0.8
    elif x > 140 and x <= 190:
        return 0.5
    elif x > 190 and x <= 365:
        return 0.3
    else:
        return 0.01
    
def feature_engineer():
    df = pd.read_csv('RealEstateData.csv',sep='\t') 
    Punish = []
    for index,row in df.iterrows():
        Punish.append(punish(row.DaysOnMarket))

    # set month sold values
    posted = df.DatePosted
    sold = df.DateSold
    seasonPosted = np.array([int(i[:2]) for i in list(posted)])
    seasonSold = np.array([int(i[:2]) for i in list(sold)])
    df['MonthPosted'] = seasonPosted
    df['MonthSold'] = seasonSold
    a=pd.get_dummies(df["MonthPosted"],drop_first=True,dtype='int64')
    a=a.rename({2:"Posted_Feb",
                3:"Posted_Mar",
                4:"Posted_Apr",
                5:"Posted_May",
                6:"Posted_June",
                7:"Posted_July",
                8:"Posted_Aug",
                9:"Posted_Sept",
                10:"Posted_Oct",
                11:"Posted_Nov",
                12:"Posted_Dec"},axis=1)
    b = pd.get_dummies(df['MonthSold'],drop_first=True,dtype=int)
    b=b.rename({2:"Sold_Feb",
                3:"Sold_Mar",
                4:"Sold_Apr",
                5:"Sold_May",
                6:"Sold_June",
                7:"Sold_July",
                8:"Sold_Aug",
                9:"Sold_Sept",
                10:"Sold_Oct",
                11:"Sold_Nov",
                12:"Sold_Dec"},axis=1,)
    df=df.join(a)
    df=df.join(b)

    df['Punish'] = Punish

    df['Marketability'] = df.SoldPrice * df.DaysOnMarket * df.Punish
    df['Marketability'] = df['Marketability'] / max(df.Marketability)

    df.drop(["City","Zipcode","DateSold","MonthPosted","MonthSold",'DatePosted','Punish'], axis=1, inplace=True)
    return df

if __name__ == "__main__":
    df = feature_engineer()
    target = df.Marketability
    # OUR OVERALL TRAINING/TESTING SPLITS
    X_train,X_test,y_train,y_test = tts(df,
                                        target,
                                        test_size=0.2,
                                        random_state=40)

    # TARGETS FOR PREDICTING PRICE AND DAYS 
    yPrice = X_test.SoldPrice
    yDays = X_test.DaysOnMarket

    # Our data for the target values
    X_for_Days = X_train.drop(["DaysOnMarket",
                               "Marketability"],axis=1)
    X_for_Price = X_train.drop(["SoldPrice",
                                "DaysOnMarket",
                                "Marketability"],axis=1)

    # Training/testing sets for finding DAYS
    XDays_train = X_train.drop(["DaysOnMarket",
                                "Marketability"],axis=1)
    XDays_test = X_test.drop(["DaysOnMarket",
                              "Marketability"],axis=1)
    yDays_train = X_train.DaysOnMarket
    yDays_test = X_test.DaysOnMarket

    # Training/testing sets for finding PRICE
    XPrice_train = X_train.drop(["SoldPrice"],axis=1)
    XPrice_test = X_test.drop(["SoldPrice"],axis=1)
    yPrice_train = X_train.SoldPrice
    yPrice_test = X_test.SoldPrice

    # ----------------------------------- Support Vector Machines -----------------------------------------

    # Gridsearch for SVMs, which didn't work properly...
    pipe = Pipeline([('gbr',svm.SVR())])
    param_grid = [{'gbr__kernel':['linear', 'poly', 'rbf', 'sigmoid','precomputed'],
                   'gbr__gamma':np.arange(0.01,1,0.04),
                   'gbr__shrinking':[True,False],
                   'gbr__degree':np.arange(3,7,1),
                   'gbr__coef0':np.arange(0,4,1),
                   'gbr__epsilon':np.arange(0.01,0.11,0.02),
                   'gbr__C':np.arange(0.1,3,0.5)}]
    gs = GridSearchCV(pipe, 
                      param_grid, 
                      cv=3,
                      verbose=1,
                      n_jobs=-1)
    gs.fit(XPrice_train,yPrice_train)
    print(gs.best_params_,gs.best_score_)
    print(gs.score(XPrice_test,yPrice_test))
    print('-'*42)


    model = svm.SVR(gamma='scale')
    model.fit(XPrice_train,yPrice_train)
    print(model.score(XPrice_test,yPrice_test))
    print('-'*42)


    model = svm.SVR(gamma='scale')
    model.fit(XDays_train,yDays_train)
    print(model.score(XDays_test,yDays_test))
    print('-'*42)

    # ----------------------------------- K-Nearest Neighbors Regression -----------------------------------------

    model = KNeighborsRegressor()
    model.fit(XPrice_train,yPrice_train)
    print(model.score(XPrice_test,yPrice_test))
    print('-'*42)


    model = KNeighborsRegressor()
    model.fit(XDays_train,yDays_train)
    print(model.score(XDays_test,yDays_test))
    print('-'*42)


    # ----------------------------------- Kernel Ridge Regressor -----------------------------------------


    model = KernelRidge()
    model.fit(XPrice_train,yPrice_train)
    print(model.score(XPrice_test,yPrice_test))
    print('-'*42)

    model = KernelRidge()
    model.fit(XDays_train,yDays_train)
    print(model.score(XDays_test,yDays_test))
    print('-'*42)


    # ----------------------------------- Partial Least Squares Regression -----------------------------------------


    model = PLSRegression()
    model.fit(XPrice_train,yPrice_train)
    print(model.score(XPrice_test,yPrice_test))
    print('-'*42)

    model = PLSRegression()
    model.fit(XDays_train,yDays_train)
    print(model.score(XDays_test,yDays_test))
    print('-'*42)

    # ----------------------------------- XGBoost Regression -----------------------------------------

    model = xgboost.XGBRegressor(learning_rate=0.2, 
                                 gamma=0,
                                 booster='dart',
                                 n_estimators=200,
                                 max_depth=6,
                                 random_state=42,
                                 reg_alpha=0,
                                 reg_lambda=1.8)
    model.fit(XPrice_train,yPrice_train)
    print('Price accuracy score:',model.score(XPrice_test,yPrice_test))
    print('-'*42)


    model = xgboost.XGBRegressor(learning_rate=0.2, 
                                 gamma=0,
                                 booster='dart',
                                 n_estimators=200,
                                 max_depth=6,
                                 random_state=42,
                                 reg_alpha=0,
                                 reg_lambda=1.8)
    model.fit(XDays_train,yDays_train)
    print('Price accuracy score:',model.score(XDays_test,yDays_test))
    print('-'*42)

    # ----------------------------------- Gradient Boosting Trees Regressor -----------------------------------------

    # GBT gridsearch. Definitely not re-running this again...
    pipe = Pipeline([('gbr',GradientBoostingRegressor(n_estimators=500,max_depth=6))])
    param_grid = [{'gbr__loss':['ls', 'lad', 'huber', 'quantile'],
                   'gbr__learning_rate':np.arange(0.1,1.1,0.1),
                   'gbr__alpha':np.linspace(0.1,0.99,8),
                   'gbr__warm_start':[True,False]}]
    gs = GridSearchCV(pipe, 
                      param_grid, 
                      cv=7,
                      verbose=1,
                      n_jobs=-1)
    gs.fit(XPrice_train,yPrice_train)
    print(gs.best_params_,gs.best_score_)
    print(gs.score(XPrice_test,yPrice_test))
    print("Most important features:\n")
    print(XPrice_train.columns[model.feature_importances_>0.05])


    # prev: {'gbr__alpha': 0.99, 'gbr__learning_rate': 0.30000000000000004, 'gbr__loss': 'huber', 'gbr__warm_start': False}
    gs = GradientBoostingRegressor(n_estimators=1000,max_depth=6,alpha=0.99,learning_rate=0.4,loss='huber',warm_start=True)
    gs.fit(XPrice_train,yPrice_train)
    print(gs.score(XPrice_test,yPrice_test))
    print('-'*42)

    # prev: {'gbr__alpha': 0.99, 'gbr__learning_rate': 0.30000000000000004, 'gbr__loss': 'huber', 'gbr__warm_start': False}
    gs = GradientBoostingRegressor(n_estimators=1000,max_depth=6,alpha=0.99,learning_rate=0.4,loss='huber',warm_start=True)
    gs.fit(XDays_train,yDays_train)
    print(gs.score(XDays_test,yDays_test))
    print('-'*42)

