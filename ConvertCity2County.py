import numpy as np
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException


counties = []            
browser = webdriver.Chrome("C:\Program Files (x86)\Google\Chrome\chromedriver.exe")
browser.get("http://www.uscounties.com/zipcodes/search.pl")

def EnterZip(zipcode):
    search_bar = browser.find_element_by_class_name("inputSize")
    search_bar.clear()
    #enter in the search query and search
    search_bar.send_keys(zipcode)
    search_bar.send_keys(Keys.RETURN)

zips = {}
for zipcode in pd.read_csv("Zipcodes.csv",index_col=0).as_matrix():
    zcode = str(zipcode[0])
    if zcode in zips:
        counties.append(zips[zcode])
        continue
    EnterZip(zcode)
    try:
        county = browser.find_element_by_tag_name("p").find_elements_by_tag_name("td")[7].text
    except:
        county = "NAN"
        
    counties.append(county)
    zips[zcode] = county
    
pd.DataFrame(counties, columns=["County"]).to_csv("counties.csv",index=False)
browser.close()
    
    
    